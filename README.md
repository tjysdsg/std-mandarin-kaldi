0. Create symlinks for aishell2 at `aishell2/wav` and for magicdata at `magicdata/wav`
1. Run `generate_data.sh` to generate a subset of the aishell2+magicdata consisting of (relatively) standard Mandarin
2. Enter `scripts/` and run `run.sh`